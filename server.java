import java.io.*;   //DataInputStream için
import java.nio.*;
import java.net.*;  //serversocket ve socket için


public class Server{

  public static void main(String[] args){
    ServerSocket ss = new ServerSocket(4407); //bir nesne-oluşturduk socket için verdiğimiz parametre port numarası
    Socket s = ss.accept();   //onaylanma
    DataInputStream dis = new DataInputStream(s.getInputStream()); //veri akşını içeri alıyor. Socket kısmından gelenleri fonksiyon vasıtasıyla parametre olarak atadık
    Strin msg = (String) dis.readUTF(); //dışarıdan gelen mesajı DataInputStreamle oku. Değer integer gelirse diye de (string) ile convert ettik
    System.out.println(msg);
    ss.close();
  }
}
