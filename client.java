import java.io.*;
import java.nio.*;
import java.net.*;
public class Client{

  public static void main (String[] args) throws Exception{
    Socket s = new Socket ("localhost" , 4407); //iletişim için server'ın adresini verdik parametre olarak. localhost = ip adresi
    DataOutputStream dos = new DataOutputStream(s.getOutputStream()); // veri akışını dışarı çıkarmak için
    dos.writeUTF("Merhaba Server");
    dos.flush(); //çıkış yaptırmak için
    dos.close();
    s.close();
  }
}

//flush(): bilgisayardaki her talimat sırayla yapılır ve bu talimatlar sıra beklerken ara bellekte tutulur.
//Biz burada io sınıfından stream(akış) olan mesajın çıkışını yaptırmak için kullanıyoruz.
